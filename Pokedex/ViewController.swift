//
//  ViewController.swift
//  Pokedex
//
//  Created by Sergio Andres Rodriguez Castillo on 02/10/23.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = .red
    }
}

